// Nicholas Krzenski
package main
import "fmt"
import "os"
import "strconv"

const box_size int = 9
const total_workers int = 15

// Data structure to represent a box
type Box struct {
	box_number int
	request int
	contents [box_size]int
	number_filled int
}

/* Read --> Workers --> Mallo --> Finisher(main) */
func main() {
	// Main input and output channels of the assembly line
	input := make(chan Box)	// Change this to buffered channel for different output
	output := make(chan Box)

	// Read the requests
	go read_requests(input)

	// Setup workers
	for i := 0; i < total_workers; i++ {
		out := make(chan Box)	// Change this to buffered channel for different output
		go worker(input, out, i)
		input = out
	}

	// Mallo cups
	go mallo(input, output)

	// Finisher
	for k := range output {
		fmt.Println("\x1B[32mCOMPLETED\033[0m", k)
	}
}

func worker( in <-chan Box, out chan<- Box, position int ) {
	// Receive box
	fmt.Println("position", position)
	for box := range in {
		// Add candy if needed
		if( (box.number_filled < box_size) && (((box.request >> uint32(position)) & 1) == 1) ) {
			box.contents[box.number_filled] = 1<<uint32(position)	// Determines 2^position
			box.number_filled += 1
		}

		// Send box to out channel
		fmt.Println("    Filling", box, "at position", position)
		out<-box
	}
	close(out)
}

func mallo( in <-chan Box, out chan<- Box ) {
	// Receive box
	for box := range in {
		// Add a mallo cup to any empty slot
		for i := 0; i < box_size; i++ {
			if( box.contents[i] == 0 ) {
				box.contents[i] = -1
				box.number_filled += 1
			}
		}

		// Send box to out channel
		fmt.Println("\x1B[33mFinishing box\033[0m", box)
		out<-box
	}
	close(out)
}

func read_requests( out chan<- Box ) {
	for i := 1; i < len(os.Args); i++ {
		request, err := strconv.Atoi(os.Args[i])
		check(err)

		// Create the request package and send to out channel
		box := Box{request: request}
		box.box_number = i
		fmt.Println("Beginning new box", i)
		out<-box
	}
	close(out)
}

func check( e error ) {
	if e != nil {
		panic(e)
	}
}
